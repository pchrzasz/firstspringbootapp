#!/bin/bash

# Deploy folder structure should look like:

# -- project

# ---- dev
# ------ resources
# -------- application.yml
# ------ initServer.log
# ------ my-app-jar


# COMMAND LINE VARIABLES
# enviroment FIRST ARGUMENT
# Ex: dev | test
env=$1

# deploy port SECOND ARGUMENT
# Ex: 8090 | 8091
serverPort=$2

# THIRD ARGUMENT deploy folder name
deployName=$3

# FOURTH ARGUMENT jar name
jarName=$4

# FIFTH ARGUMENT external config file name
# Ex: application-localhost.yml
configFile=$5


#### CONFIGURABLE VARIABLES ######
# destination absolute path. It must be pre created
sourFile=$WORKSPACE/target/$jarName*.jar
sourConfigFolder=$WORKSPACE/$configFolder*
destAbsPath=/home/ec2-user/projects/$deployName/$env
configFolder=resources
destFile=$destAbsPath/$jarName.jar
destConfigFolder=$destAbsPath/$configFolder
properties=--spring.config.location=$destAbsPath/$configFolder/$configFile

#CONSTANTS
logFile=initServer.log
dstLogFile=$destAbsPath/$logFile
whatToFind="Started "
msgLogFileCreated="$logFile created"
msgBuffer="Buffering: "
msgAppStarted="Application Started... exiting buffer!"

### FUNCTIONS
##############
function stopServer(){
    echo " "
    echo "Stoping process on port: $serverPort"
    fuser -n tcp -k $serverPort > redirection &
    echo " "
}

function deleteFiles(){
    echo "Deleting $destFile"
    rm -rf $destFile

    echo "Deleting $destConfigFolder"
    rm -rf $destConfigFolder

    echo "Deleting $dstLogFile"
    rm -rf $dstLogFile

    echo " "
}

function createDeployDirectory(){
    echo "Creating directory $destAbsPath"
    mkdir -p $destAbsPath

    echo "Creating directory $destConfigFolder"
    mkdir -p $destConfigFolder

    echo "Creating file $dstLogFile"
    touch $dstLogFile

    echo " "
}

function copyFiles(){
    echo "Copying files from $sourFile"
    cp $sourFile $destFile

    echo "Copying files from $sourConfigFolder"
    cp -r $sourConfigFolder $destConfigFolder

    echo " "
}

function run(){
    nohup nice java -jar $destFile --server.port=$serverPort $properties $> $dstLogFile 2>&1 &

    echo "COMMAND: nohup nice java -jar $destFile --server.port=$serverPort $properties $> $dstLogFile 2>&1 &"

    echo " "
}

function changeFilePermission(){
    echo "Changing File Permission: chmod 777 $destAbsPath"
    chmod 777 $destAbsPath

    echo "Changing File Permission: chmod 777 $destConfigFolder"
    chmod 777 $destConfigFolder

    echo "Changing File Permission: chmod 777 $destFile"
    chmod 777 $destFile

    echo "Changing File Permission: chmod 777 $dstLogFile"
    chmod 777 $dstLogFile

    echo " "
}

function watch(){

    tail -f $dstLogFile |

        while IFS= read line
            do
                echo "$msgBuffer" "$line"

                if [[ "$line" == *"$whatToFind"* ]]; then
                    echo $msgAppStarted
                    pkill  tail
                fi
        done
}

### FUNCTIONS CALLS
#####################
# /path/to/this/file/api-deploy.sh dev 8082 spring-boot application-localhost.yml

# 1 - stop server on port
stopServer

# 2 - delete destinations folder content
deleteFiles

# 3 - create deploy directory
createDeployDirectory

# 4 - copy files to deploy dir
copyFiles

changeFilePermission

# 5 - start server
run

# 6 - watch loading messages until  ($whatToFind) message is found
watch
