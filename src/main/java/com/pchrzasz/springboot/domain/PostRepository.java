package com.pchrzasz.springboot.domain;

import org.springframework.data.repository.CrudRepository;

public interface PostRepository
    extends CrudRepository<Post, Long>
{
}
